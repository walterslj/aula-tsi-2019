<?php



class Usuario {
    private $id;
    private $nome;
    private $email; 
    private $senha; 
    private $objDb; 

    
    public function __construct(){

        // echo "aqui esta a conexao com o banco\n";

        $this->objDb = new mysqli('Localhost','root', '','bd_tsi');

    }

    public function setId (int $id) {
        $this -> id = $id;
    }

    public function setNome (string $nome) {
        $this -> nome = $nome;
    }

    public function setEmail (string $email) {
        $this -> email = $email;
    }

    public function setSenha (string $senha) {
        $this -> senha = password_hash($senha, PASSWORD_DEFAULT);
    }

    public function getId (int $id) : int {
        return $this -> id;
    }

    public function getNome (string $nome) : string {
        return $this -> nome;
    }

    public function getEmail (string $email) : string {
        return $this -> email;
    }

    public function getSenha (string $senha) : string {
        return $this -> senha;
    }
    
    public function saveUsuario(){

        $objStmt = $this->objDb->prepare('REPLACE INTO login (id, nome, email, senha) VALUES(?,?,?,?)');

        $objStmt->bind_param('isss', $this->id, $this->nome, $this->email, $this->senha);
        
        if($objStmt->execute() ){
            return true;
        }else{
            return false;
        }
    }

    public function Apagar(){

        $objStmt = $this->objDb->prepare('DELETE FROM login WHERE id = ?');

        $objStmt->bind_param('i', $this->id);

        if($objStmt->execute() ){
            return true;
        }else{
            return false;
        }
    }   

    public function Ver(){

        $objStmt = $this->objDb->prepare('SELECT * FROM login where(?)');

        $objStmt->bind_param('i', $this->id);

        // $ver = ;

        if($objStmt->execute() ){

            return $objStmt;

        }else{

            return false;
        }
    }


    public function __destruct() {
        // echo "<br>Fechando conexao com SGDB";
        unset($this->objDb);
        
    }


}