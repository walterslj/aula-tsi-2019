<?php

echo "<pre>";
// constante no php
define('QTD_PAGINAS',10);

echo "\nvalor da minha constante é: ". QTD_PAGINAS;

// variavel para passar valor para uma constante
$ip_do_banco = '192.168.45.12';

define('IP_DO_BANCO',10);

echo "\nO IP do SGBD é" . IP_DO_BANCO;
echo "\nentou na linha: " . __LINE__;
echo "\nentou na linha: " . __FILE__;

echo "\n\n";
// muito bom para depurar o codigo
var_dump($ip_do_banco);

/*
agora fica mais legal 
é a hora do vetor
arrey*/

echo "\n\n";
$dias = ['dom',
        'seg',
        'ter',
        'qua',
        'qui',
        'sex',
        'sab'];

var_dump($dias);
unset($dias);


$dias[0] = 'dom';
$dias[1] = 'seg';
$dias[2] = 'ter';
$dias[3] = 'qua';
$dias[4] = 'qui';
$dias[5] = 'sex';
$dias[6] = 'sab';

var_dump($dias);



$dias = array(  0=>'dom',
                1=>'seg',
                2=>'ter',
                3=>'qua',
                4=>'qui',
                5=>'sex',
                6=>'sab');


var_dump($dias);















echo "<pre>";